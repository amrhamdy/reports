import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewRowPopupComponent } from './add-new-row-popup.component';

describe('AddNewRowPopupComponent', () => {
  let component: AddNewRowPopupComponent;
  let fixture: ComponentFixture<AddNewRowPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewRowPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewRowPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
