import { CdkTableModule } from '@angular/cdk/table';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatMenuModule } from '@angular/material/menu';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatTableModule,
    CdkTableModule,
    MatMenuModule
  ],
  exports: [
    MatTableModule,
    CdkTableModule,
    MatMenuModule
  ]
})
export class SharedModule { }
