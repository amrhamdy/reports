import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'legal-reports';
  displayedColumns = ['DocName', 'Type', 'Classification', 'Auther', 'PublishedDate', 'Status', 'Actions'];
  dataSource = ELEMENT_DATA;
}
export interface ICaseRowData {
  DocName: string;
  Type: string;
  Classification: string;
  Auther: string;
  PublishedDate: string,
  Status: string
}
const ELEMENT_DATA: ICaseRowData[] = [
  { DocName: 'DocumentName no.1', Type: 'Policy', Classification: 'Classification1', Auther: 'Abdullah', PublishedDate: '22/10/2019', Status: 'Published' },
  { DocName: 'DocumentName no.1', Type: 'Policy', Classification: 'Classification1', Auther: 'Abdullah', PublishedDate: '22/10/2019', Status: 'Rejected' },
  { DocName: 'DocumentName no.1', Type: 'Policy', Classification: 'Classification1', Auther: 'Abdullah', PublishedDate: '22/10/2019', Status: 'Waiting Approval' },
  { DocName: 'DocumentName no.1', Type: 'Policy', Classification: 'Classification1', Auther: 'Abdullah', PublishedDate: '22/10/2019', Status: 'Approved' },
  { DocName: 'DocumentName no.1', Type: 'Policy', Classification: 'Classification1', Auther: 'Abdullah', PublishedDate: '22/10/2019', Status: 'Draft' },
  { DocName: 'DocumentName no.1', Type: 'Policy', Classification: 'Classification1', Auther: 'Abdullah', PublishedDate: '22/10/2019', Status: 'Waiting Approval' },
  { DocName: 'DocumentName no.1', Type: 'Policy', Classification: 'Classification1', Auther: 'Abdullah', PublishedDate: '22/10/2019', Status: 'Approved' }
];